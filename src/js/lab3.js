const countries = require('i18n-iso-countries');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const isValidPhoneNumber = require('libphonenumber-js');
const { randomUserMock, additionalUsers } = require('./mock_for_L3');

function getUsers(randomUsers, otherUsers) {
  return randomUsers.map((randomUser) => {
    const { phone, email, gender } = randomUser;

    const resultUser = {
      gender,
      title: randomUser.name.title,
      full_name: randomUser.name.first.concat(' ', randomUser.name.last),
      city: randomUser.location.city,
      state: randomUser.location.state,
      country: randomUser.location.country,
      postcode: randomUser.location.postcode,
      coordinates: randomUser.location.coordinates,
      timezone: randomUser.location.timezone,
      email,
      b_date: randomUser.dob.date,
      age: randomUser.dob.age,
      phone,
      picture_large: randomUser.picture.large,
      picture_thumbnail: randomUser.picture.picture_thumbnail,
      id: null,
      favorite: false,
      bg_color: null,
      course: null,
      note: null,
    };

    const additionalProps = ['id', 'favorite', 'bg_color', 'note', 'course'];

    otherUsers.forEach((additionalUser) => {
      if (additionalUser.full_name === resultUser.full_name) {
        additionalProps.forEach((propertyName) => {
          resultUser[propertyName] = additionalUser[propertyName];
        });
      }
    });
    return resultUser;
  });
}

function validateString(valueString) {
  if (typeof valueString === 'string'
        && valueString[0] === valueString[0].toUpperCase()) { return true; }
  return false;
}

function validatePhoneNumber(number, country) {
  const countryCode = countries.getAlpha2Code(country, 'en');
  if (typeof number !== 'string' || !isValidPhoneNumber(number, countryCode)) return false;
  const parsedNumber = phoneUtil.parse(number, countryCode);

  return phoneUtil.isValidNumberForRegion(parsedNumber, countryCode);
}

function validate(user) {
  let result = true;

  if (!validatePhoneNumber(user.phone, user.country)) { result = false; }

  const properties = ['full_name', 'gender', 'note', 'state', 'city', 'country'];

  properties.forEach((property) => {
    if (!validateString(user[property])) { result = false; }
  });

  if (typeof user.age !== 'number'
            && (user.email === undefined || !user.email.includes('@'))) { result = false; }
  return result;
}

function filter(users, country, age, gender, favorite) {
  return users.filter((user) => user.country === country
                        || user.country === country
                        || user.age === age
                        || user.gender === gender
                        || user.favorite === favorite);
}

function find(users, propertyName, value) {
  return users.filter((user) => user[propertyName] === value);
}

const sortOrder = Object.freeze({
  desc: 1,
  asc: 2,
});

function sort(users, propertyName, order) {
  if (order === sortOrder.asc) {
    return users.sort((a, b) => (a[propertyName] > b[propertyName] ? 1 : -1));
  }
  return users.sort((a, b) => (a[propertyName] < b[propertyName] ? 1 : -1));
}

function percentage(users, predicate) {
  let passed = 0;
  users.forEach((user) => {
    if (predicate(user)) { passed += 1; }
  });

  return (passed / users.length) * 100;
}

function main() {
  const users = getUsers(randomUserMock, additionalUsers);
  const user = users[0];

  console.log(`Validation: ${validate(user)}`);
  console.log('Filter');
  filter(users, 'Iran', 53, 'Male', true).map((u) => console.log(u));
  console.log('Find: ');
  find(users, 'country', 'Norway').map((u) => console.log(u));
  console.log('Sorting: ');
  sort(users, 'full_name', sortOrder.desc).map((u) => console.log(u));
  console.log(`Percentage: ${percentage(users, (u) => u.age < 48)}`);
}

main();
