const testModules = require('./test-module');
require('../css/app.css');

/** ******** Your code here! *********** */

const countries = require('i18n-iso-countries');
const libphonenumber = require('libphonenumber-js');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const { randomUserMock, additionalUsers } = require('./mock_for_L3');

const getUsers = (randomUsers, otherUsers) => randomUsers.map((randomUser) => {
  const { phone, email, gender } = randomUser;

  const resultUser = {
    gender,
    title: randomUser.name.title,
    full_name: randomUser.name.first.concat(' ', randomUser.name.last),
    city: randomUser.location.city,
    state: randomUser.location.state,
    country: randomUser.location.country,
    postcode: randomUser.location.postcode,
    coordinates: randomUser.location.coordinates,
    timezone: randomUser.location.timezone,
    email,
    b_date: randomUser.dob.date,
    age: randomUser.dob.age,
    phone,
    picture_large: randomUser.picture.large,
    picture_thumbnail: randomUser.picture.thumbnail,
    picture_medium: randomUser.picture.medium,
    id: null,
    favorite: false,
    bg_color: null,
    course: null,
    note: null,
  };

  const additionalProps = ['id', 'favorite', 'bg_color', 'note', 'course'];

  otherUsers.forEach((additionalUser) => {
    if (additionalUser.full_name === resultUser.full_name) {
      additionalProps.forEach((propertyName) => {
        resultUser[propertyName] = additionalUser[propertyName];
      });
    }
  });
  return resultUser;
});

const validateString = (valueString) => {
  if (typeof valueString === 'string'
    && valueString[0] === valueString[0].toUpperCase()) { return true; }
  return false;
};

const validatePhoneNumber = (number, country) => {
  const countryCode = countries.getAlpha2Code(country, 'en');
  if (typeof number !== 'string' || !libphonenumber.isValidPhoneNumber(number, countryCode)) return false;
  const parsedNumber = phoneUtil.parse(number, countryCode);

  return phoneUtil.isValidNumberForRegion(parsedNumber, countryCode);
};

const validate = (user) => {
  let result = true;
  // if (!validatePhoneNumber(user.phone, user.country)) { result = false; }

  const properties = ['full_name', 'city', 'country'];

  properties.forEach((property) => {
    if (!validateString(user[property])) { result = false; }
  });

  if (typeof user.age !== 'number'
    && (user.email === undefined || !user.email.includes('@'))) { result = false; }
  return result;
};

const filtersState = {
  gender: null,
  ageFrom: null,
  ageTo: null,
  country: null,
  fav: null,
};

function filter(users, filtersState) {
  return users
    .filter((user) => (filtersState.country == null ? true : user.country == filtersState.country))
    .filter((user) => (filtersState.ageTo == '' ? true : user.age <= filtersState.ageTo))
    .filter((user) => (filtersState.ageFrom == '' ? true : user.age >= filtersState.ageFrom))
    .filter((user) => (filtersState.gender == null ? true : user.gender == filtersState.gender))
    .filter((user) => (filtersState.fav == null ? true : user.favorite == filtersState.fav));
}

function find(users, value) {
  let match = false;
  return users.filter((user) => {
    match = false;
    Object.keys(user).forEach((key) => {
      if ((key == 'full_name' || key == 'note') & match == false) {
        match = user[key] == null ? false : user[key].toLowerCase().includes(value.toLowerCase());
      } else if (key == 'age' & match == false) {
        match = user[key] == Number.parseInt(value);
      }
    });
    return match;
  });
}

const sortOrder = Object.freeze({
  desc: 1,
  asc: 2,
});

function sort(users, propertyName, order) {
  if (order === sortOrder.asc) {
    return users.sort((a, b) => (a[propertyName] > b[propertyName] ? 1 : -1));
  }
  return users.sort((a, b) => (a[propertyName] < b[propertyName] ? 1 : -1));
}

function percentage(users, predicate) {
  let passed = 0;
  users.forEach((user) => {
    if (predicate(user)) { passed += 1; }
  });

  return (passed / users.length) * 100;
}

const addToFavsBtn = document.createElement('button');
addToFavsBtn.className = 'pink-button';

const add = 'Add to favorites';
const remove = 'Remove from favorites';

const showVerbose = (user) => {
  const popup = document.getElementById('info-popup');
  const info = document.querySelector('#info-popup > figure > figcaption > div');
  const name = document.querySelector('#info-popup > figure > figcaption > span');
  const photo = document.querySelector('#info-popup > figure > div > img');
  const note = document.querySelector('#info-popup > p');
  info.innerHTML = `<p>${user.city}, ${user.country}</p>
  <p>${user.age}, ${user.gender}</p>
  <a class="blue">${user.email}</a>
  <p>${user.phone}</p>`;

  addToFavsBtn.innerHTML = user.favorite ? remove : add;

  addToFavsBtn.onclick = () => {
    if (user.favorite) {
      user.favorite = false;
      addToFavsBtn.innerHTML = add;
      alert('User was removed from favorites!');
      name.className = '';
    } else {
      user.favorite = true;
      addToFavsBtn.innerHTML = remove;
      alert('User was added to favorites!');
      name.className = 'star';
    }
  };

  popup.appendChild(addToFavsBtn);

  name.textContent = user.full_name;

  photo.src = user.picture_large;
  note.textContent = user.note || '';
  popup.style.visibility = 'visible';
};
const drawUsers = (users) => {
  const filteredUsers = filter(users, filtersState);
  console.log(filtersState);
  console.log(filteredUsers);
  const top = document.querySelector('.top');
  top.removeChild(top.lastChild);

  const topList = document.createElement('div');
  topList.className = 'top-list';
  filteredUsers.forEach((user) => {
    const figure = document.createElement('figure');
    const layout = `<div class="image-frame">
         <img src="${user.picture_medium}" alt="Photo ${user.full_name}">
      </div>
      <figcaption>
         <p>${user.full_name}</p>
         <p class="country">${user.country}</p>
      </figcaption>`;
    figure.innerHTML = layout;
    figure.addEventListener('click', () => {
      showVerbose(user);
    });
    topList.appendChild(figure);
  });
  top.appendChild(topList);
};

const createDropDownFilter = (options, parentElement) => {
  const defaultOption = document.createElement('option');
  defaultOption.textContent = 'All';
  defaultOption.selected = true;
  parentElement.appendChild(defaultOption);

  options.forEach((option) => {
    const optionItem = document.createElement('option');
    optionItem.textContent = option;
    parentElement.appendChild(optionItem);
  });
};

const genderFilter = document.getElementById('gender-select');

const countryFilter = document.getElementById('country-select');
const ageFilterFrom = document.getElementById('ageFrom');
const ageFilterTo = document.getElementById('ageTo');

const fillFilters = (users, filtersState) => {
  const filters = users.reduce((acc, user) => {
    acc.gender.add(user.gender);
    acc.country.add(user.country);
    return acc;
  },
  {
    gender: new Set(),
    country: new Set(),
    fav: [true, false],
  });
  createDropDownFilter(filters.gender, genderFilter);
  createDropDownFilter(filters.country, countryFilter);
  ageFilterFrom.value = Math.min(filters.age);
  ageFilterTo.value = Math.max(filters.age);

  document.querySelector('#filters > a.close')
    .addEventListener('click', () => {
      document.getElementById('filters').style.visibility = 'hidden';
    });

  document.getElementById('openFiltersBtn')
    .addEventListener('click', () => {
      document.getElementById('filters').style.visibility = 'visible';
    });

  const favsFilter = document.getElementById('only_favs_flrt');

  const filterChangeListener = () => {
    const defaultOption = 'All';
    filtersState.gender = genderFilter.value == defaultOption ? null : genderFilter.value.toLowerCase();
    filtersState.country = countryFilter.value == defaultOption ? null : countryFilter.value;
    filtersState.fav = favsFilter.checked == false ? null : true;
    filtersState.ageFrom = ageFilterFrom.value;
    filtersState.ageTo = ageFilterTo.value;
    document.dispatchEvent(new Event('filter-change'));
  };

  genderFilter.addEventListener('change', filterChangeListener);
  countryFilter.addEventListener('change', filterChangeListener);
  favsFilter.addEventListener('change', filterChangeListener);
  ageFilterFrom.addEventListener('change', filterChangeListener);
  ageFilterTo.addEventListener('change', filterChangeListener);
};

const searchField = document.getElementById('search');
const searchBtn = document.getElementById('search-btn');

let users = getUsers(randomUserMock, additionalUsers);

const order = {
  1: 'full_name',
  2: 'age',
  3: 'gender',
  4: 'country',
};

const fillTable = (users) => {
  const table = document.querySelector('table');
  table.removeChild(table.lastChild);
  const tbody = document.createElement('tbody');
  users.forEach((user) => {
    const row = document.createElement('tr');

    for (const num in order) {
      const cell = document.createElement('td');
      cell.innerHTML = user[order[num]];
      row.appendChild(cell);
    }
    tbody.appendChild(row);
  });
  table.appendChild(tbody);
};

fillTable(users);

const user = users[0];
document.querySelector('#info-popup > a.close')
  .addEventListener('click', () => {
    document.getElementById('info-popup').style.visibility = 'hidden';
  });

const setDefaultFilters = () => {
  filtersState.ageFrom = 0;
  filtersState.ageTo = 99;
};

setDefaultFilters();
fillFilters(users, filtersState);
drawUsers(users);
document.addEventListener('filter-change', () => {
  drawUsers(users, filtersState);
});

document.addEventListener('search', () => {
  users = filter(
    users,
    filtersState
  );
  drawUsers(users);
});

searchBtn.addEventListener('click', () => {
  drawUsers(find(users, searchField.value));
});
const addUser = () => {
  const name = document.getElementById('name').value;
  const countries = document.getElementById('countries');
  const country = countries.options[countries.selectedIndex].text;
  const city = document.getElementById('city').value;
  const number = document.getElementById('number').value;
  const email = document.getElementById('email').value;
  const age = document.getElementById('age').value;
  let gender;
  if (document.getElementById('m-radio').checked) {
    gender = document.getElementById('m-radio').value;
  } else {
    gender = document.getElementById('f-radio').value;
  }

  const newUser = {
    full_name: name,
    country,
    city,
    phone: number,
    email,
    age: parseInt(age),
    gender,
  };

  if (validate(newUser)) {
    users.push(newUser);
    drawUsers(users);
  }
};
const addBtn = document.getElementById('add-btn');
const addForm = document.getElementById('add-popup');
addBtn.addEventListener('click', addUser);
const showAddForm = document.getElementById('add-form-btn');
showAddForm.addEventListener('click', () => {
  addForm.style.visibility = 'visible';
});

document.querySelector('#add-popup > a.close')
  .addEventListener('click', () => {
    addForm.style.visibility = 'hidden';
  });

const sortOptions = document.querySelectorAll('.sortOptions > input');
sortOptions.forEach((radio) => {
  radio.addEventListener('click', () => drawUsers(sort(users, radio.value, sortOrder.asc)));
});
